package com.example.fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment1.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment1 : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private  lateinit var communicator:Communicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_fragment1, container, false)
         val num_one:Button=view.findViewById(R.id.num_one)
        val num_two: Button = view.findViewById(R.id.num_two)
        val num_three: Button = view.findViewById(R.id.num_three)
        val num_four: Button = view.findViewById(R.id.num_four)
        val num_five: Button =view.findViewById(R.id.num_five)
        val num_six: Button = view.findViewById(R.id.num_six)
        val num_seven: Button = view.findViewById(R.id.num_seven)
        val num_eight: Button = view.findViewById(R.id.num_eight)
        val num_nine: Button = view.findViewById(R.id.num_nine)
        val num_zero: Button = view.findViewById(R.id.num_zero)
        val num_go: Button = view.findViewById(R.id.num_go)
        val num_dot: Button = view.findViewById(R.id.num_dot)
        val num_back: Button = view.findViewById(R.id.num_back)
        val num_clear: Button = view.findViewById(R.id.num_clear)

        val weight: TextView = view.findViewById(R.id.weight)
        val height: TextView = view.findViewById(R.id.height)

        var w1=weight.text.toString()
        var h1=height.text.toString()

        weight.setOnClickListener {
            weight.isSelected = true
            height.isSelected = false
            height.setTextColor(Color.parseColor("#A9A9A9"))
            weight.setTextColor(Color.parseColor("#FF7433"))
            if (weight.isSelected) {

                num_one.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "1"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_two.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "2"
                    w1 = buclick1
                    weight.setText(buclick1)

                }
                num_three.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "3"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_four.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "4"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_five.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "5"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_six.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "6"
                    w1 = buclick1
                    weight.setText(buclick1)

                }
                num_seven.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "7"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_eight.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "8"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_nine.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "9"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_zero.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "0"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_dot.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "."
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_back.setOnClickListener {

                    var str = weight.text.toString()
                    if (str == null || str.isEmpty()) {

                    } else {
                        w1 = ""
                        val new = str.substring(0, str.length - 1)

                        weight.setText(new)
                        w1 = weight.text.toString()


                    }
                }
                num_clear.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    weight.setText("")
                }
            }

        }
        height.setOnClickListener {
            height.isSelected = true
            weight.isSelected = false
            weight.setTextColor(Color.parseColor("#A9A9A9"))
            height.setTextColor(Color.parseColor("#FF7433"))
            if (height.isSelected) {
                num_one.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "1"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_two.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "2"
                    h1 = buclick2
                    height.setText(buclick2)

                }
                num_three.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "3"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_four.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "4"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_five.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "5"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_six.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "6"
                    h1 = buclick2
                    height.setText(buclick2)

                }
                num_seven.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "7"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_eight.setOnClickListener {
                    var buclick2 = weight.text.toString()
                    buclick2 += "8"
                    h1 = buclick2
                    weight.setText(buclick2)
                }
                num_nine.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "9"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_zero.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "0"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_dot.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "."
                    h1 = buclick2
                    height.setText(buclick2)

                }
                num_back.setOnClickListener {

                    var str = height.text.toString()
                    if (str == null || str.isEmpty()) {

                    } else {
                        val new = str.substring(0, str.length - 1)
                        h1 = ""
                        height.setText(new)
                        h1 = height.text.toString()


                    }
                }
                num_clear.setOnClickListener {
                    var buclick2 = height.text.toString()
                    height.setText("")
                }
            }

        }

        communicator=activity as Communicator
        num_go.setOnClickListener {//try to different activity


           // val result_bmi: TextView = view.findViewById(R.id.bmi)
            h1 = (h1.toDouble() / 100.0).toString()
           // var result_id: TextView = view.findViewById(R.id.result_id)
            val n1 = h1.toDouble() * h1.toDouble()
            val resultt = w1.toDouble() / n1.toDouble()
            var re = "%.2f".format(resultt).toDouble()
           //result_bmi.setText(re.toString())

            Log.d("MainActivity", "$re")
            communicator.passDataCom(re.toString())
        }

        return view

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragment1.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
            Fragment1().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}
