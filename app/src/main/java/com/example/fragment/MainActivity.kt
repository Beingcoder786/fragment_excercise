package com.example.fragment

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit

class MainActivity : AppCompatActivity(), Communicator {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val firstFragment = Fragment1()//instance create
        val secondFragment = Fragment2()
        val btn1: Button = findViewById(R.id.but1)
       /* val btn2: Button = findViewById(R.id.bt2)*/

        supportFragmentManager.beginTransaction().apply {  //initially first fragment will show
            replace(R.id.fl_fragment, Fragment1())
            commit()

        }


        btn1.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fl_fragment, Fragment1())
                commit()
            }
        }

        /*   btn2.setOnClickListener {
               supportFragmentManager.beginTransaction().apply {
                   replace(R.id.fl_fragment,secondFragment)
                   commit()
               }
           }*/


    }

    override fun passDataCom(result: String) {

        val bundle = Bundle()
        bundle.putString("message", result)

        val transaction = this.supportFragmentManager.beginTransaction()
        val fragmentB = Fragment2()
        fragmentB.arguments = bundle

        transaction.replace(R.id.fl_fragment, fragmentB)
        transaction.commit()
    }
}